/**
 * 
 */
package co.verisoft.retry;

import co.verisoft.retry.RepeatFailedTestExtension;
import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Retention(RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@ExtendWith(RepeatFailedTestExtension.class)
@TestTemplate


/**
 * @author galln
 *
 */
public @interface RetryOnFail {
	
	int value();

}
