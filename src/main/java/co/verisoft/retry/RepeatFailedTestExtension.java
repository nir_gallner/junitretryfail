package co.verisoft.retry; /**
 *
 */
import static java.util.Spliterator.ORDERED;
import static java.util.Spliterators.spliteratorUnknownSize;
import static java.util.stream.Collectors.joining;
import static java.util.stream.StreamSupport.stream;
import static org.junit.platform.commons.util.AnnotationUtils.findAnnotation;
import static org.junit.platform.commons.util.AnnotationUtils.isAnnotated;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ExtensionContext.Namespace;
import org.junit.jupiter.api.extension.ExtensionContext.Store;


import org.junit.jupiter.api.extension.TestExecutionExceptionHandler;
import org.junit.jupiter.api.extension.TestTemplateInvocationContext;
import org.junit.jupiter.api.extension.TestTemplateInvocationContextProvider;

/**
 * @author Nir Gallner
 *
 */
public class RepeatFailedTestExtension
        implements TestTemplateInvocationContextProvider, TestExecutionExceptionHandler, AfterEachCallback {


    private static final Namespace NAMESPACE = Namespace.create("co", "verisoft", "RepeatFailedTestExtension");

    /*
     * (non-Javadoc)
     *
     * @see org.junit.jupiter.api.extension.TestExecutionExceptionHandler#
     * handleTestExecutionException(org.junit.jupiter.api.extension.
     * ExtensionContext, java.lang.Throwable)
     */
    @Override
    public void handleTestExecutionException(ExtensionContext context, Throwable throwable) throws Throwable {
        // this `context` (M) is a child of the context passed to
        // `provideTestTemplateInvocationContexts` (T),
        // which means M's store content is invisible to T's store; this can be fixed by
        // using T's store here
        // TODO: throw proper exception
        //ExtensionContext templateContext = context.getParent().orElseThrow(IllegalStateException::new);
        repeaterFor(context).failed(throwable);

        boolean hasNext = repeaterFor(context).hasNext();
        context.getStore(NAMESPACE).put(context.getRequiredTestMethod().getName(), hasNext);

    }

    /*
     * (non-Javadoc)
     *
     * @see org.junit.jupiter.api.extension.TestTemplateInvocationContextProvider#
     * supportsTestTemplate(org.junit.jupiter.api.extension.ExtensionContext) Will
     * return true if it is for monitor purpose and has the proper annotation
     */
    @Override
    public boolean supportsTestTemplate(ExtensionContext context) {

        Method method = context.getRequiredTestMethod();
        Optional<Class<?>> testClass = context.getTestClass();
        String className = context.getRequiredTestClass().getSimpleName();

        //return className.startsWith("Monitor_") && isAnnotated(method, RetryOnFail.class);
        return isAnnotated(method, RetryOnFail.class) ||isAnnotated(testClass, RetryOnFail.class) ;

    }

    /*
     * (non-Javadoc)
     *
     * @see org.junit.jupiter.api.extension.TestTemplateInvocationContextProvider#
     * provideTestTemplateInvocationContexts(org.junit.jupiter.api.extension.
     * ExtensionContext)
     */
    @Override
    public Stream<TestTemplateInvocationContext> provideTestTemplateInvocationContexts(ExtensionContext context) {
        FailedTestRepeater repeater = repeaterFor(context);
        return stream(spliteratorUnknownSize(repeater, ORDERED), false);
    }

    private static FailedTestRepeater repeaterFor(ExtensionContext context) {
        Method repeatedTest = context.getRequiredTestMethod();

        return context.getStore(NAMESPACE).getOrComputeIfAbsent(repeatedTest.toString(),
                __ -> FailedTestRepeater.createFor(repeatedTest), FailedTestRepeater.class);
    }

    // TODO: evaluate switching this back to private later on
    public static class FailedTestRepeater implements Iterator<RepeatFailedTestInvocationContext> {

        private final int maxRepetitions;
        private int repetitionsSoFar;
        private final List<Throwable> exceptionsSoFar;

        private FailedTestRepeater(int maxRepetitions) {
            this.maxRepetitions = maxRepetitions;
            this.repetitionsSoFar = 0;
            this.exceptionsSoFar = new ArrayList<>();
        }

        static FailedTestRepeater createFor(Method repeatedTest) {
            int maxRepetitions = findAnnotation(repeatedTest, RetryOnFail.class).map(RetryOnFail::value)
                    .orElseThrow(IllegalStateException::new);
            return new FailedTestRepeater(maxRepetitions);
        }

        void failed(Throwable exception) throws Throwable {
            // TODO: throw if in "always fail" mode, log otherwise
            exceptionsSoFar.add(exception);

            boolean allRepetitionsFailed = exceptionsSoFar.size() == maxRepetitions;
            if (allRepetitionsFailed)
                failTest();
        }

        private void failTest() {
            // TODO provide proper message - like stack traces
            String message = exceptionsSoFar.stream().map(Throwable::getMessage).collect(joining("\n"));
            throw new AssertionError(message);
        }

        @Override
        public boolean hasNext() {
            // there's always at least one execution
            if (repetitionsSoFar == 0)
                return true;

            // if we caught an exception in each repetition, each repetition failed,
            // including the previous one
            boolean previousFailed = repetitionsSoFar == exceptionsSoFar.size();
            boolean maxRepetitionsReached = repetitionsSoFar == maxRepetitions;
            return previousFailed && !maxRepetitionsReached;
        }

        @Override
        public RepeatFailedTestInvocationContext next() {
            repetitionsSoFar++;
            return new RepeatFailedTestInvocationContext();
        }

    }

    @Override
    public void afterEach(ExtensionContext context) throws Exception {
        ExtensionContext templateContext = context.getParent().orElseThrow(IllegalStateException::new);
        boolean hasNext = repeaterFor(templateContext).hasNext();
        System.setProperty("hasNext", String.valueOf(hasNext));

        if (hasNext) {
            System.setProperty("restartDriver", "true");
        } else if (context.getExecutionException().isPresent()) {
            System.setProperty("restartDriver", "true");
        } else {
            System.setProperty("restartDriver", "false");
        }

    }


    private static Store getStore(ExtensionContext context) {
        return context.getRoot().getStore(NAMESPACE);
    }


}

