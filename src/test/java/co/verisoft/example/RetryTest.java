package co.verisoft.example;

import co.verisoft.retry.RetryOnFail;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@RetryOnFail(3)
public class RetryTest {

    @Test
    public void shouldFail(){
        Assertions.fail("test fail");
    }

    @Test
    public void shouldPass(){
        Assertions.assertTrue(true);
    }
}
